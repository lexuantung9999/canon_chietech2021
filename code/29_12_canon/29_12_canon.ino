#include <Arduino.h>

// LiquidCrystal I2C - Version: Latest 
// #include <LiquidCrystal_I2C.h>

// motor
#define mor1_pwm 9  // dong co sort - TIM2
#define mor1_in1 50
#define mor1_in2 51

#define mor2_pwm 11 // dong co lap -TIM1
#define mor2_in1 46
#define mor2_in2 47

#define mor3_pwm 10 // dong co xoay - TIM2
#define mor3_in1 48
#define mor3_in2 49

// endstop
#define sort_up_ens    A2
#define sort_down_ens  A3
#define assem_up_ens   A0
#define assem_down_ens A1

//relay
#define relay1_ 22 // dai oc 
#define relay2_ 23 // vong dem
#define relay3_ 24 // bulong
#define relay4_ 25 // lap

#define sort_bt 2
#define assem_bt 3
#define auto_bt 19


#define sensor1 26 //dai oc
#define sensor2 27 //dem phang
#define sensor3 28//dem venh
#define sensor4 29 //bu long

int mode = 0, component_stt=0, sort_stt=0, assem_stt=0, sort_move=0, assem_move=0;
char command;
int speed_sort =50, speed_assem=26;
unsigned long counter_sort, counter_assem;

void config_TIM3(); 
void config_TIM4();
void config_TIM5();
void sort_up(int speeds);
void sort_down(int speeds);
void sort_stop();
void assem_up(int speeds);
void assem_down(int speeds);
void assem_stop();
void sort_ISR();
void assem_ISR();
void auto_ISR();
void xoay();
void dung_xoay();
void stop_all();
void reset_counter();
void homing();

void sort_auto();
void assem_auto();

void sort_auto(){
     assem_stop();
      sort_down(70);
      delay(200);
      sort_down(speed_sort);
      delay(200);
      int i=speed_sort;
      while(digitalRead(sort_down_ens)!=0){
            i=i-1;
            if(i<=20)i=20;            
            sort_down(i);
      }
      sort_stop();
      delay(1200);
      sort_up(70);
      delay(300);
      sort_up(speed_sort);
      while(digitalRead(sort_up_ens)!=0);
      sort_stop();
      delay(1200);
}

void assem_auto(){

      Serial.println("Assem");
      sort_stop();
      delay(50);
      digitalWrite(relay3_,1); digitalWrite(relay1_,1);  digitalWrite(relay2_,1);  
      delay(350);
      digitalWrite(relay3_,0);
      delay(300);
      digitalWrite(relay1_,0);  digitalWrite(relay2_,0);  
      delay(1000);
      xoay();
      assem_down(60);
      delay(950);
      assem_down(speed_assem);
      while(digitalRead(assem_down_ens)!=0);
      assem_stop();
      delay(50);
      dung_xoay();
      delay(100);
      //while(digitalRead(assem_down_ens)!=0);
      digitalWrite(relay4_,1);
      delay(1000);
      digitalWrite(relay4_,0);
      delay(50);
      assem_up(50);
      while(digitalRead(assem_up_ens)!=0);
      assem_stop();
      delay(1500);
}
void config_TIM3(){
    // 0.1s timer confirguration
    TCCR3A = 0;
    TCCR3B = 0;
    TIMSK3 = 0;

    TCCR3B |= (1 << WGM32) | (1 << CS31) | (1 << CS30); // prescale = 64 and CTC mode 
    OCR3A = 24999;              // initialize OCR1
    TIMSK3 = (1 << OCIE3A);     // Output Compare Interrupt Enable Timer/Counter1 channel 
}

void config_TIM4(){
    // 0.1s timer confirguration
    TCCR4A = 0;
    TCCR4B = 0;
    TIMSK4 = 0;

    TCCR4B |= (1 << WGM42) | (1 << CS41) | (1 << CS40); // prescale = 64 and CTC mode 
    OCR4A = 24999;              // initialize OCR1
    TIMSK4 = (1 << OCIE4A);     // Output Compare Interrupt Enable Timer/Counter1 channel 
}

void config_TIM5(){
    // 0.1s timer confirguration
    TCCR5A = 0;
    TCCR5B = 0;
    TIMSK5 = 0;

    TCCR5B |= (1 << WGM52) | (1 << CS51) | (1 << CS50); // prescale = 64 and CTC mode 
    OCR5A = 24999;              // initialize OCR1
    TIMSK5 = (1 << OCIE5A);     // Output Compare Interrupt Enable Timer/Counter1 channel 
}

void sort_ISR(){
  //stop_all();
  mode =1;
  //homing();
  Serial.println("sort");  
  //Serial.println(mode);
}

void assem_ISR(){
  
  //stop_all();
  mode =2;
  //homing();
  Serial.println("assem");
  //Serial.println(mode);
}

void auto_ISR(){
  //stop_all();
  mode =3;
  //homing();
  Serial.println("auto");
  //Serial.println(mode); 
}

void xoay(){
  digitalWrite(mor3_in1,1);
  digitalWrite(mor3_in2,0);
  analogWrite(mor3_pwm,255);  
  //stop_all();
}

void dung_xoay(){
  digitalWrite(mor3_in1,0);
  digitalWrite(mor3_in2,0);
  analogWrite(mor3_pwm,255);
  
}


void sort_up(int speeds){
  sort_move =1;
  digitalWrite(mor1_in1,1);
  digitalWrite(mor1_in2,0);
  analogWrite(mor1_pwm,speeds); // sort len
}

void sort_down(int speeds){
  sort_move =-1;
  digitalWrite(mor1_in1,0);
  digitalWrite(mor1_in2,1);
  analogWrite(mor1_pwm,speeds); // sort xuong
}
void sort_stop(){
  sort_move=0;
  digitalWrite(mor1_in1,0);
  digitalWrite(mor1_in2,0);
  analogWrite(mor1_pwm,255); // sort xuong
}


void assem_down(int speeds){
  assem_move=-1;
  digitalWrite(mor2_in1,0);
  digitalWrite(mor2_in2,1);
  analogWrite(mor2_pwm,speeds); // lap len
}

void assem_up(int speeds){
  assem_move=1;
  digitalWrite(mor2_in1,1);
  digitalWrite(mor2_in2,0);
  analogWrite(mor2_pwm,speeds); // lap xuong
 }

void assem_stop(){
  assem_move=0;
  digitalWrite(mor2_in1,0);
  digitalWrite(mor2_in2,0);
  analogWrite(mor2_pwm,255); // lap xuong
}

void stop_all(){
  dung_xoay();
  assem_stop();
  sort_stop();
}

void homing(){
//  while(digitalRead(sort_down_ens)!=0 or digitalRead(assem_down_ens)!=0){
//    if (digitalRead(sort_down_ens)!=0){
//      sort_down(speed_sort);
//    }
//    else if (digitalRead(sort_down_ens)==0) sort_stop();
//    
//    if (digitalRead(assem_down_ens)!=0){
//      assem_down(speed_assem);
//    }
//    else if (digitalRead(assem_down_ens)==0) assem_stop();
//  }
    mode=0;
    while(digitalRead(sort_up_ens)!=0){
      sort_up(speed_sort);  
    }
    sort_stop();

    while(digitalRead(assem_up_ens)!=0){
        assem_up(55);  
      }
    assem_stop();
//        digitalWrite(relay4_,1);
//      delay(1000);
//      digitalWrite(relay4_,0);
//      delay(50);
    Serial.println("Homing Done");
}

void reset_all_pin(){
    digitalWrite(relay1_,0);  digitalWrite(relay2_,0);  digitalWrite(relay3_,0);  digitalWrite(relay4_,0);  
}

void setup() {
  // put your setup code here, to run once:
  delay(500);
  cli();
  Serial.begin(115200);
  pinMode(relay1_, OUTPUT); pinMode(relay2_, OUTPUT); pinMode(relay3_, OUTPUT); pinMode(relay4_, OUTPUT); 
  pinMode(sort_up_ens, INPUT_PULLUP); pinMode(sort_down_ens, INPUT_PULLUP); pinMode(assem_up_ens, INPUT_PULLUP); pinMode(assem_down_ens, INPUT_PULLUP); 
  pinMode(mor1_pwm,OUTPUT); pinMode(mor2_pwm,OUTPUT); pinMode(mor3_pwm,OUTPUT); 
  pinMode(mor1_in1,OUTPUT); pinMode(mor2_in1,OUTPUT); pinMode(mor3_in1,OUTPUT);
  pinMode(mor1_in2,OUTPUT); pinMode(mor2_in2,OUTPUT); pinMode(mor3_in2,OUTPUT);
  pinMode(sensor1,INPUT); pinMode(sensor2,INPUT); pinMode(sensor3,INPUT); pinMode(sensor4,INPUT);
  pinMode(13,OUTPUT);
  pinMode(auto_bt,INPUT_PULLUP); pinMode(assem_bt,INPUT_PULLUP); pinMode(sort_bt, INPUT_PULLUP);
  digitalWrite(relay1_,0);  digitalWrite(relay2_,0); digitalWrite(relay3_,0); digitalWrite(relay4_,0);
  homing();
  attachInterrupt(digitalPinToInterrupt(sort_bt),sort_ISR,FALLING);
  attachInterrupt(digitalPinToInterrupt(assem_bt),assem_ISR,FALLING);
  attachInterrupt(digitalPinToInterrupt(auto_bt),auto_ISR,FALLING);
 // config_TIM3(); 
//  config_TIM4(); 
//  config_TIM5(); 
  sei();
  mode=0;
  //xoay();
//  delay(1000);
//  digitalWrite(relay1_,1);  digitalWrite(relay2_,1);  digitalWrite(relay3_,1);  digitalWrite(relay4_,1); 
//  delay(2000);
  //digitalWrite(relay1_,0);  digitalWrite(relay2_,0);  digitalWrite(relay3_,0);  digitalWrite(relay4_,0); 
  
//  while(digitalRead(sort_up_ens)!=0);
//  sort_stop();    
//  delay(2000);

//        digitalWrite(relay3_,1);
//      delay(1000);
//      digitalWrite(relay3_,0);
//      delay(1000);



    
}
int count_assem=0;
void loop() {
  Serial.println(mode);
  
    //sort
    while(mode==1){
     sort_auto();
       // Serial.println("Mode1");
       //mode=1;
    }

    //Asssem
    while(mode==2){
      assem_auto();
     mode=0;
    }

    while(mode==3){
          if(digitalRead(sensor1)==1 &&  digitalRead(sensor2)==1 && digitalRead(sensor3)==1 && digitalRead(sensor4)==1){
              count_assem =5;
              sort_stop();
//              for(int i=0;i<5;i++){
//                assem_auto();
//              }
              while(count_assem){
                  if(digitalRead(sensor1)==1 &&  digitalRead(sensor2)==1 && digitalRead(sensor3)==1 && digitalRead(sensor4)==1){
                    count_assem=5;  
                  }
                  assem_auto();
                  count_assem-=1;
                  Serial.println(count_assem);
              }
          }

          else{
              assem_stop();
              sort_auto();
          }
      mode=3;
    }

//assem_auto();
}

//    Serial.print(digitalRead(sensor1));
//    Serial.print(digitalRead(sensor2));
//    Serial.print(digitalRead(sensor3));
//    Serial.println(digitalRead(sensor4));


//    Serial.print(digitalRead(assem_up_ens));
//    Serial.print(digitalRead(assem_down_ens));
//    Serial.print(digitalRead(sort_up_ens));
//    Serial.println(digitalRead(sort_down_ens));


  //Serial.print(digitalRead(assem_up_ens));
//    Serial.print(digitalRead(sort_bt));
//   Serial.print(digitalRead(assem_bt));
//    Serial.println(digitalRead(auto_bt));
//}

//ISR (TIMER3_COMPA_vect){
////  if(digitalRead(sensor1)==1) sort_ISR();
////  if(digitalRead(sensor2)==1) assem_ISR();
////  if(digitalRead(sensor3)==1) auto_ISR();
//}
////
//ISR (TIMER4_COMPA_vect){
////   if(mode==1){
////    Serial.println("TIM3");
////  }
//}
//
//ISR (TIMER5_COMPA_vect){
//  //digitalWrite(13,!digitalRead(13));
////  if(digitalRead(sensor1)==1 && digitalRead(sensor2)==1 && digitalRead(sensor3)==1 && digitalRead(sensor4)==1){
////    component_stt = 1;
////  }
////  else component_stt = 0;
////  Serial.println(component_stt);
//}
