#define sole  2

#define pwm_xoay 10
#define xoay_1  9
#define xoay_2  8


#define pwm_lap 5
#define lap_1  6
#define lap_2  7

#define endstop A0
void setup() {
  // put your setup code here, to run once:
  pinMode(sole,OUTPUT);
  pinMode(xoay_1,OUTPUT);
  pinMode(xoay_2,OUTPUT);
  pinMode(pwm_xoay,OUTPUT);

  pinMode(lap_1,OUTPUT);
  pinMode(lap_2,OUTPUT);
  pinMode(pwm_lap,OUTPUT);
  pinMode(A0,INPUT_PULLUP);

  Serial.begin(115200);

  home();
  xoay();
//  xoay();
//  lap();
//  delay(5000);
//  stop();
//  digitalWrite(sole,1);
//  delay(1000);
//  digitalWrite(sole,0);
  lap();
  delay(2300);
  stop();
  delay(1000);
  lap();
  delay(500);
  stop();
  
  analogWrite(pwm_xoay,0);
  analogWrite(pwm_lap,0);
 delay(2000);
  digitalWrite(sole,1);
  delay(2000);
  digitalWrite(sole,0);
  delay(1000);
  
  home();

 
}

void home(){
   nha();
   while(digitalRead(endstop)==1);
   stop();
   delay(1000);
   lap();
   delay(100);
   stop();
   analogWrite(pwm_xoay,0);
  analogWrite(pwm_lap,0);
}
void xoay(){
 digitalWrite(xoay_1,0); digitalWrite(xoay_2,1); 
  analogWrite(pwm_xoay,255);  
}

void nha(){
  digitalWrite(lap_1,0); digitalWrite(lap_2,1); 
  analogWrite(pwm_lap,100);  
}
void lap(){
  digitalWrite(lap_1,1); digitalWrite(lap_2,0); 
  analogWrite(pwm_lap,100);  
}
void stop(){
    digitalWrite(lap_1,0); digitalWrite(lap_2,0); 
  analogWrite(pwm_xoay,255); 
  }
void loop() {
  // put your main code here, to run repeatedly:
  //Serial.println(digitalRead(A0));
}
